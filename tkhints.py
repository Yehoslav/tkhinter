#!/usr/bin/env nix-shell
#!nix-shell -i python3 -p python310 python310Packages.tkinter
"""
The GPLv3 License (GPLv3)

Copyright (c) 2023 Yehoward Rudenco <rudencoyehoward@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import subprocess

import tkinter as tk


class HintsError(Exception):
    pass


def data_to_object(data: str) -> dict:
    data_lines = [line.strip(" ") for line in data.split("\n") if line.strip(" ") != ""]

    keys_data = {}

    if len(data_lines) == 0:
        return keys_data

    for i, line in enumerate(data_lines):
        if line[0] == "*":
            keys_data["title"] = line[1:].strip(" ")
            continue

        key, *desc = line.split("-")
        if len(desc) == 0:
            raise HintsError(
                f"""Unexpected line at {i+1}

If thou wilt needs marry, marry a fool; for wise men know well enough what
monsters you make of them.
               
Maybe you forgot a '-' or a '*'"""
            )

        skey = key.strip(" ")
        sdesc = "-".join(desc).strip(" ")

        if len(skey) == 0:
            raise HintsError(f"Key expected at line: {i+1}\n\t{line}")
        if len(sdesc) == 0:
            raise HintsError(f"Description expected at line: {i+1}\n\t{line}")

        keys_data["keys"] = keys_data.get("keys", []) + [(skey, sdesc)]

    return keys_data


def key_description_lbl(
    master: tk.Tk | tk.Frame, key: str, description: str, colors: dict
) -> tk.Frame:
    """
    Generates a key-descrption Widget to give the possibility to use different
    colors for each element.
    """

    frame = tk.Frame(master)
    frame.configure(bg=colors["bg"])
    key_lbl = tk.Label(
        frame,
        text=key,
        fg=colors["key"],
        font=("Fira Code Nerd Font", 10),
        bg=colors["bg"],
    )
    description_lbl = tk.Label(
        frame,
        text="- " + description,
        fg=colors["description"],
        font=("Fira Code Nerd Font", 10),
        bg=colors["bg"],
    )

    key_lbl.pack(side=tk.LEFT)
    description_lbl.pack(side=tk.LEFT)

    return frame


def get_xrdb_colors() -> dict:
    xrdb = subprocess.check_output(["xrdb", "-query"])
    xrdb_str = xrdb.decode()
    xrdb_list = [
        tuple(map(lambda x: x.strip(), line.split(":")[:2]))
        for line in xrdb_str.split("\n") 
        if line.strip() != ''
    ]
    # print(xrdb_list)
    xrdb_colors = {k: val for k, val in xrdb_list}
    return {
        "bg": xrdb_colors["*.background"],
        "title_bg": xrdb_colors["*.color0"],
        "key": xrdb_colors["*.color12"],
        "description": xrdb_colors["*.color7"],
        "title_fg": xrdb_colors["*.foreground"],
    }


def create_app(
    key_bindings: list,
    win_title: str,
):
    """
    Creates a Tkinter window that displays the keybind with their respective description as given.

    @param key_bindings:
    @param win_title: The title of the window, usualy the description of the KeyChord
    """

    colors = get_xrdb_colors()

    root = tk.Tk()
    root.title("Qtile hints")
    root.configure(bg=colors["bg"])

    tk.Label(
        root,
        text=win_title,
        font=("Fira Code Nerd Font", 14),
        fg=colors["title_fg"],
        bg=colors["title_bg"],
    ).pack(fill=tk.X)

    key_frame = tk.Frame(root)
    key_frame.configure(bg=colors["bg"])
    key_frame.pack()

    cols = 6
    row = 0
    col = 0
    for key in key_bindings:
        msg_lbl = key_description_lbl(
            master=key_frame, key=key[0], description=key[1], colors=colors
        )
        msg_lbl.grid(row=row, column=col, sticky=tk.W, ipadx=10)
        col += 1
        if col == cols:
            row += 1
            col = 0

    if col == 0:
        row -= 1

    height = 24 + (1 + row) * 18 + 20

    top = root.winfo_screenheight() - height
    root.geometry(f"{root.winfo_screenwidth()-20}x{height}+10+{top-10}")

    return root


def main():

    key_data = sys.stdin.read()

    keys = data_to_object(key_data)

    app = create_app(keys["keys"], keys["title"])

    app.mainloop()


if __name__ == "__main__":
    main()
